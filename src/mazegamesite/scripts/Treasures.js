const Treasures = [
    {name: "Gold", tValue: 100},
    {name: "Silver", tValue: 60},
    {name: "Bronze", tValue: 30}
];

Object.freeze(Treasures);

const getTreasure = function (type) {
    "use strict";
    let treasure = undefined;

    switch (type) {
        case Treasures[0].name:
            treasure = Treasures[0];
            break;
        case Treasures[1].name:
            treasure = Treasures[1];
            break;
        case Treasures[2].name:
            treasure = Treasures[2];
            break;
    }
    return treasure;
};