﻿const Maze = [
    //Room 0
    [
        //0,0
        [
            {x: 0, y: 0, threat: false, threatType: "n/a", treasure: true, treasureType: "Bronze", bumped: false, passage: false },
            {x: 1, y: 0, threat: true, threatType: "Werewolf", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 0, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 0, threat: false, threatType: "n/a", treasure: true, treasureType: "Bronze", bumped: false, passage: false },
            {x: 4, y: 0, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 6, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,1
        [
            {x: 0, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,2
        [
            {x: 0, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,3
        [
            {x: 0, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 3, threat: true, threatType: "Werewolf", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,4
        [
            {x: 0, y: 4, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 4, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 4, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 4, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
        ],

        //0,5
        [
            {x: 0, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,6
        [
            {x: 0, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,7
        [
            {x: 0, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 7, threat: true, threatType: "Vampire", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,8
        [
            {x: 0, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 8, threat: false, threatType: "n/a", treasure: true, treasureType: "Gold", bumped: false, passage: false },
            {x: 8, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //0,9
        [
            {x: 0, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 1, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 9, threat: false, threatType: "n/a", treasure: true, treasureType: "Silver", bumped: false, passage: false },
            {x: 3, y: 9, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 9, threat: true, threatType: "Vampire", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 9, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 9, threat: true, threatType: "Troll", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ]
    ],

    //Room 1
    [
        //1,0
        [
            {x: 0, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 0, threat: false, threatType: "n/a", treasure: true, treasureType: "Silver", bumped: false, passage: false },
            {x: 2, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 0, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 5, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 0, threat: true, threatType: "Werewolf", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 0, threat: false, threatType: "n/a", treasure: true, treasureType: "Bronze", bumped: false, passage: false },
        ],

        //1,1
        [
            {x: 0, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 1, threat: true, threatType: "Vampire", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,2
        [
            {x: 0, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 2, threat: false, threatType: "n/a", treasure: true, treasureType: "Bronze", bumped: false, passage: false },
            {x: 6, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 2, threat: true, threatType: "Vampire", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,3
        [
            {x: 0, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,4
        [
            {x: 0, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 4, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,5
        [
            {x: 0, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 5, threat: true, threatType: "Werewolf", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,6
        [
            {x: 0, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,7
        [
            {x: 0, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 7, threat: false, threatType: "n/a", treasure: true, treasureType: "Gold", bumped: false, passage: false },
            {x: 8, y: 7, threat: true, threatType: "Troll", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,8
        [
            {x: 0, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 8, threat: false, threatType: "n/a", treasure: true, treasureType: "Bronze", bumped: false, passage: false },
            {x: 5, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //1,9
        [
            {x: 0, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 1, y: 9, threat: true, threatType: "Troll", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 9, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 4, y: 9, threat: true, threatType: "Vampire", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 9, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ]
    ],

    //Room 2
    [
        //2,0
        [
            {x: 0, y: 0, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 0, threat: true, threatType: "Troll", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 0, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 5, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 0, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,1
        [
            {x: 0, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 1, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 1, threat: true, threatType: "Werewolf", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 1, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,2
        [
            {x: 0, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 2, threat: false, threatType: "n/a", treasure: true, treasureType: "Bronze", bumped: false, passage: false },
            {x: 8, y: 2, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 2, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,3
        [
            {x: 0, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 3, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 3, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,4
        [
            {x: 0, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 4, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 4, threat: false, threatType: "n/a", treasure: true, treasureType: "Silver", bumped: false, passage: false },
            {x: 6, y: 4, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 4, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,5
        [
            {x: 0, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 5, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 5, threat: true, threatType: "Troll", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 9, y: 5, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,6
        [
            {x: 0, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 6, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 6, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,7
        [
            {x: 0, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 7, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 7, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,8
        [
            {x: 0, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 8, threat: false, threatType: "n/a", treasure: true, treasureType: "Gold", bumped: false, passage: false },
            {x: 2, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 3, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 8, threat: false, threatType: "n/a", treasure: true, treasureType: "Bronze", bumped: false, passage: false },
            {x: 6, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 8, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 8, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ],

        //2,9
        [
            {x: 0, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 1, y: 9, threat: true, threatType: "Werewolf", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 2, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: true },
            {x: 3, y: 9, threat: false, threatType: "Wall", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 4, y: 9, threat: true, threatType: "Vampire", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 5, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 6, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 7, y: 9, threat: true, threatType: "Troll", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 8, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
            {x: 9, y: 9, threat: false, threatType: "n/a", treasure: false, treasureType: "n/a", bumped: false, passage: false },
        ]
    ]
];
