/*Threats.js*/
QUnit.test("testGetThreat",function( assert ){
    let getOne = getThreat("Troll");
    let get2 = getThreat("Werewolf");
    let get3 = getThreat("Vampire");
    let getDefault = getThreat("default");

    assert.strictEqual(getOne, Threats[0], "Troll object passed!");
    assert.strictEqual(get2, Threats[1], "Werewolf object passed!");
    assert.strictEqual(get3, Threats[2], "Vampire object passed!");
    assert.strictEqual(getDefault, undefined, "Default passed!");

    assert.strictEqual(getOne.name, Threats[0].name, "Troll name passed!");
    assert.strictEqual(get2.name, Threats[1].name, "Werewolf name passed!");
    assert.strictEqual(get3.name, Threats[2].name, "Vampire name passed!");

    assert.strictEqual(getOne.weakTo, Threats[0].weakTo, "Troll weakTo passed!");
    assert.strictEqual(get2.weakTo, Threats[1].weakTo, "Werewolf weakTo passed!");
    assert.strictEqual(get3.weakTo, Threats[2].weakTo, "Vampire weakTo passed!");
});

/*Treasures.js*/
QUnit.test("testGetTreasure",function( assert ){
    let get1 = getTreasure("Gold");
    let get2 = getTreasure("Silver");
    let get3 = getTreasure("Bronze");
    let getDefault = getTreasure("default");

    assert.strictEqual(get1, Treasures[0], "Gold object passed!");
    assert.strictEqual(get2, Treasures[1], "Silver object passed!");
    assert.strictEqual(get3, Treasures[2], "Bronze object passed!");
    assert.strictEqual(getDefault, undefined, "Default object passed!");

    assert.strictEqual(get1.name, Treasures[0].name, "Gold name passed!");
    assert.strictEqual(get2.name, Treasures[1].name, "Silver name passed!");
    assert.strictEqual(get3.name, Treasures[2].name, "Bronze name passed!");

    assert.strictEqual(get1.tValue, Treasures[0].tValue, "Gold tValue passed!");
    assert.strictEqual(get2.tValue, Treasures[1].tValue, "Silver tValue passed!");
    assert.strictEqual(get3.tValue, Treasures[2].tValue, "Bronze tValue passed!");
});

/*Weapons.js*/
QUnit.test("testGetWeapon",function( assert ){
    let get1 = getWeapon("Club");
    let get2 = getWeapon("Silver Sword");
    let get3 = getWeapon("Holy Water");
    let getDefault = getWeapon("default");

    assert.strictEqual(get1, Weapons[0], "Club object passed!");
    assert.strictEqual(get2, Weapons[1], "SilverSword object passed!");
    assert.strictEqual(get3, Weapons[2], "HolyWater object passed!");
    assert.strictEqual(getDefault, undefined, "Default object passed!");

    assert.strictEqual(get1.name, Weapons[0].name, "Club name passed!");
    assert.strictEqual(get2.name, Weapons[1].name, "Silver Sword name passed!");
    assert.strictEqual(get3.name, Weapons[2].name, "Holy Water name passed!");

    assert.strictEqual(get1.kills, Weapons[0].kills, "Club kills passed!");
    assert.strictEqual(get2.kills, Weapons[1].kills, "Silver Sword kills passed!");
    assert.strictEqual(get3.kills, Weapons[2].kills, "Holy Water kills passed!");
});

/*Setup.js*/
QUnit.test("testSetup",function( assert ){
    let setupUndefined = setup();
    assert.strictEqual(setupUndefined, undefined, "Setup No-Args passed!");

    let setupMazeVersion1 = setup("room", 1);
    let setupMazeVersion2 = setup("room", 2);
    let setupMazeVersion3 = setup("room", 3);
    let setupMazeVersionDefault = setup("room");

    assert.strictEqual(setupMazeVersion1, Maze[0], "Setup Maze 1 passed!");
    assert.strictEqual(setupMazeVersion2, Maze[1], "Setup Maze 2 passed!");
    assert.strictEqual(setupMazeVersion3, Maze[2], "Setup Maze 3 passed!");
    assert.strictEqual(setupMazeVersionDefault, undefined, "Setup Maze default passed!");

    let setupStartVersion1 = setup("start", 1);
    let setupStartVersion2 = setup("start", 2);
    let setupStartVersion3 = setup("start", 3);
    let setupStartVersionDefault = setup("start", );
    
    assert.strictEqual(setupStartVersion1, Passages.starts[0], "Setup Start 1 passed!");
    assert.strictEqual(setupStartVersion2, Passages.starts[4], "Setup Start 2 passed!");
    assert.strictEqual(setupStartVersion3, Passages.starts[6], "Setup Start 3 passed!");
    assert.strictEqual(setupStartVersionDefault, undefined, "Setup Start default passed!");
});