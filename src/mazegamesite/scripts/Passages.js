// const getPassages = function () {
//     let passages = [];
//     let roomsIndex = -1;
//     let roomIndex = -1;
//     let rowIndex = -1;
//
//     for (let room of Maze){
//         for (let row of room) {
//             for (let cell of row){
//                 //If Cell is a passage
//                 if(cell.passage){
//                     //Build the indexes
//                     rowIndex = row.indexOf(cell);
//                     roomIndex = room.indexOf(row);
//                     roomsIndex = Maze.indexOf(room);
//
//                     //Robustness
//                     if(rowIndex !== -1 && roomIndex !== -1 && roomsIndex !== -1){
//                         console.log("Maze[" + roomsIndex + "][" + roomIndex + "][" + rowIndex +"]");
//                         passages.push(Maze[roomsIndex][roomIndex][rowIndex]);
//                     }
//                 }
//             }
//         }
//     }
//     return passages;
// };
// const Passages = getPassages();

const Passages = {
    starts: [
        Maze[0][0][5],
        Maze[0][4][9],
        Maze[0][9][0],

        Maze[1][0][4],
        Maze[1][9][0],
        Maze[1][9][3],

        Maze[2][0][4],
        Maze[2][5][8],
        Maze[2][9][2],
    ],
    exits: [
        Maze[2][0][4],
        Maze[1][9][0],
        Maze[1][0][4],
        Maze[0][9][0],
        Maze[0][4][9],
        Maze[2][5][8],
        Maze[0][0][5],
        Maze[1][9][3],
        undefined,
    ],
};