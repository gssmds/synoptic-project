﻿RouteConfig = function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "content.htm",
            controller: "MazeController"
        })
        .otherwise({
            templateUrl: "404.htm"
        });
};
