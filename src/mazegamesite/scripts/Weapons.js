const Weapons = [
    {name: "Club", kills: "Troll"},
    {name: "Silver Sword", kills: "Werewolf"},
    {name: "Holy Water", kills: "Vampire"}
];

Object.freeze(Weapons);

const getWeapon = function (name) {
    "use strict";
    let weapon = undefined;
    switch (name) {
        case Weapons[0].name:
            weapon = Weapons[0];
            break;
        case Weapons[1].name:
            weapon = Weapons[1];
            break;
        case Weapons[2].name:
            weapon = Weapons[2];
            break;
    }
    return weapon;
};