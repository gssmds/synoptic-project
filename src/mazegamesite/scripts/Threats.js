const Threats = [
    {name: "Troll", weakTo: "Club"},
    {name: "Werewolf", weakTo: "Silver Sword"},
    {name: "Vampire", weakTo: "Holy Water"},
    {name: "Wall", weakTo: "n/a"}
];

Object.freeze(Threats);

const getThreat = function (type) {
    "use strict";
    let threat = undefined;

    switch (type) {
        case Threats[0].name:
            threat = Threats[0];
            break;
        case Threats[1].name:
            threat = Threats[1];
            break;
        case Threats[2].name:
            threat = Threats[2];
            break;
    }
    return threat;
};