﻿const setup = function (request, version) {
    "use strict";
    switch (request) {
        case "room" :
            return getRoom();
        case "start" :
            return getStartCell();
        default :
            return undefined;
    }

    function getRoom() {
        "use strict";
        switch (version) {
            case 1:
                return Maze[0];
            case 2:
                return Maze[1];
            case 3:
                return Maze[2];
            default:
                return undefined;
        }
    }

    function getStartCell() {
        "use strict";
        switch (version) {
            case 1:
                return Passages.starts[0];
            case 2:
                return Passages.starts[4];
            case 3:
                return Passages.starts[6];
            default:
                return undefined;
        }
    }
};
