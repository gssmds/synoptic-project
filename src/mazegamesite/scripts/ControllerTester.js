let room = undefined;

let info = "Use the WASD or arrow keys to navigate around the maze";

let player = undefined;

let playerTreasure = [];
let wealth = 0;
let escaped = false;

const controllerSetup = function (version) {
    "use strict";
    //If version is undefined, use random number, otherwise use defined value
    version = (
        version === undefined ?
            Math.floor((Math.random() * 3) + 1) :
            version
    );
    room = setup("room", version);
    player = setup("start", version);
};

const updateInfo = function (newMessage) {
    info = newMessage;
    return info;
};

const move = function (destination) {
    "use strict";
    if(destination !== player){
        player = destination;
        return true;
    }
    return false;
};

const validateLocation = function (cell) {
    "use strict";
    let validatedLocation = cell;

    if (cell.threatType === "Wall") {
        cell.bumped = true;
        validatedLocation = player;
        updateInfo("You cannot go that way");
    }

    if(wealth > 0){
        wealth = wealth - 1;
    }

    if (cell.treasure) {
        pickupTreasure(cell);
    }

    if (cell.passage) {
        validatedLocation = goThroughPassage(cell);
    }

    return validatedLocation;
};

const goThroughPassage = function(cell) {
    "use strict";
    let index = -1;
    let passageExit = player;

    Passages.starts.forEach(function (entry) {
        if (cell === entry) {
            index = Passages.starts.indexOf(entry);
        }
    });

    switch (index) {
        case 3:
        case 4:
        case 6:
            controllerSetup(1);
            break;
        case 1:
        case 2:
        case 7:
            controllerSetup(2);
            break;
        case 0:
        case 5:
            controllerSetup(3);
            break;
        case 8:
            //Ultimate exit
            //If there are threats remaining, don't enter passage
            if (validateEscape()) {
                passageExit = Passages.exits[index];
                escaped = true;
            }
            break;
    }

    //Return linked passage.
    if (index > -1 && index < 8) {
        passageExit = Passages.exits[index];
        updateInfo("Travelled to Room " + (Maze.indexOf(room) + 1));
    }

    return passageExit;
};

const validateEscape = function() {
    "use strict";
    let message = "You escaped the Maze with " + wealth + " points!";
    let canEscape = true;

    Maze.forEach(function (room) {
        room.forEach(function (row) {
            row.forEach(function (cell) {
                if(cell.threat){
                    //Only allowed to escape once all Threats are dealt with
                    canEscape = false;
                    message = "You cannot leave while there are threats remaining";
                }
            });
        });
    });

    updateInfo(message);
    return canEscape;
};

const pickupTreasure = function (cell) {
    "use strict";
    //If Cell has treasure
    //But isn't a 'Coin'
    if (cell.treasure && cell.treasureType !== "Coin") {
        const treasure = getTreasure(cell.treasureType);
        //Pick it up
        playerTreasure.push(treasure);
        updateInfo("Picked up the " + cell.treasureType);

        //Update wealth
        wealth = wealth + treasure.tValue;

        //Remove from Cell
        cell.treasure = false;
        cell.treasureType = "n/a";
        return true;
    } else {
        return false;
    }
};

const keyboardInput = function (which) {
    "use strict";
    let destination = player;

    if (player === undefined || escaped) {
        updateInfo("The door slammed behind you and you can't go back...");
        return false;
    }

    switch (which) {
        case 65:
        case 37:
            //a or left key
            if ((player.x - 1) < 0) {
                return false;
            }
            destination = validateLocation(room[player.y][player.x - 1]);
            break;
        case 87:
        case 38:
            //w or up key
            if ((player.y - 1) < 0) {
                return false;
            }
            destination = validateLocation(room[player.y - 1][player.x]);
            break;
        case 68:
        case 39:
            //d or right key
            if ((player.x + 1) > 9) {
                return false;
            }
            destination = validateLocation(room[player.y][player.x + 1]);
            break;
        case 83:
        case 40:
            //s or down key
            if ((player.y + 1) > 9) {
                return false;
            }
            destination = validateLocation(room[player.y + 1][player.x]);
            break;
    }

    return move(destination);
};

const setDisplay = function (cell) {
    "use strict";
    //Default style of valid
    let returnClass = "Valid";

    if (cell.passage) {
        returnClass = "Passage";
    }

    //Only set wall style if the cell has been bumped.
    if(cell.bumped && cell.threatType === "Wall"){
        returnClass = "Wall";
    }

    if (cell.treasure){
        returnClass = cell.treasureType;
    }

    if (cell.threat) {
        returnClass = cell.threatType;
    }

    if (cell === player) {
        returnClass = "Player";
    }
    return returnClass;
};

const setTitle = function (cell) {
    "use strict";
    let title = setDisplay(cell);

    if(title === "Valid"){
        //Falsify Y to conform to Cartesian Co-Ordinate system
        title = cell.x + "," + (9 - cell.y);
    }

    return title;
};

const dropCoin = function () {
    "use strict";
    let message = "You drop a coin";

    //Don't allow dropping coins on Threats or Treasure
    if(player.threat || player.treasure){
        message = "You can't drop a coin here...";
    } else {
        if(wealth > 0){
            player.treasure = true;
            player.treasureType = "Coin";
            wealth = wealth - 1;
        } else {
            message = "You need a score of at least 1 to drop a coin";
        }
    }

    return message;
};

const attack = function (weaponName) {
    "use strict";
    const threatName = player.threatType;
    let message = "";
    let attackOutcome = " resisted the ";

    //If there's a threat in the current Cell
    if(player.threat === true){
        const threat = getThreat(threatName);
        const weapon = getWeapon(weaponName);

        //If the weapon kills the threat
        if (threat.weakTo === weapon.name && weapon.kills === threat.name){
            //Remove the threat
            player.threat = false;
            player.threatType = "n/a";
            attackOutcome = " was killed by the ";
        }

        message = "The " + threatName + attackOutcome + weaponName;

    } else {
        message = "You hit the ground with your " + weaponName + ". It doesn't do anything but you feel better";
    }

    return message;
};
