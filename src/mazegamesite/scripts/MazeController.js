﻿MazeController = function ($scope) {
    "use strict";

    $scope.room = undefined;

    $scope.info = "Use the WASD or arrow keys to navigate around the maze";

    $scope.player = undefined;

    $scope.playerTreasure = [];
    $scope.wealth = 0;
    $scope.escaped = false;

    $scope.setup = function (version) {
        "use strict";
        //If version is undefined, use random number, otherwise use defined value
        version = (
            version === undefined ?
                Math.floor((Math.random() * 3) + 1) :
                version
        );
        $scope.room = setup("room", version);
        $scope.player = setup("start", version);
    };

    $scope.updateInfo = function (newMessage) {
        $scope.info = newMessage;
    };

    $scope.keyboardInput = function (event) {
        "use strict";
        let destination = $scope.player;

        if ($scope.player === undefined || $scope.escaped){
            $scope.updateInfo("The door slammed behind you and you can't go back...");
            return false;
        }

        switch (event.which) {
            case 65:
            case 37:
                //a or left key
                if(($scope.player.x - 1) < 0){
                    return false;
                }
                destination = validateLocation($scope.room[$scope.player.y][$scope.player.x - 1]);
                break;
            case 87:
            case 38:
                //w or up key
                if (($scope.player.y - 1) < 0) {
                    return false;
                }
                destination = validateLocation($scope.room[$scope.player.y - 1][$scope.player.x]);
                break;
            case 68:
            case 39:
                //d or right key
                if(($scope.player.x + 1) > 9){
                    return false;
                }
                destination = validateLocation($scope.room[$scope.player.y][$scope.player.x + 1]);
                break;
            case 83:
            case 40:
                //s or down key
                if (($scope.player.y + 1) > 9) {
                    return false;
                }
                destination = validateLocation($scope.room[$scope.player.y + 1][$scope.player.x]);
                break;
        }

        return move(destination);

        function move(destination) {
            "use strict";
            $scope.player = destination;
            return true;
        }

        function validateLocation(cell) {
            "use strict";
            let validatedLocation = cell;

            if (cell.threatType === "Wall") {
                cell.bumped = true;
                validatedLocation = $scope.player;
                $scope.updateInfo("You cannot go that way");
            }

            if($scope.wealth > 0){
                $scope.wealth = $scope.wealth - 1;
            }

            if (cell.treasure) {
                pickupTreasure(cell);
            }

            if (cell.passage) {
                validatedLocation = goThroughPassage(cell);
            }

            return validatedLocation;

            function goThroughPassage(cell) {
                "use strict";
                let index = -1;
                let passageExit = $scope.player;

                Passages.starts.forEach(function (entry) {
                    if (cell === entry) {
                        index = Passages.starts.indexOf(entry);
                    }
                });

                switch (index) {
                    case 3:
                    case 4:
                    case 6:
                        $scope.setup(1);
                        break;
                    case 1:
                    case 2:
                    case 7:
                        $scope.setup(2);
                        break;
                    case 0:
                    case 5:
                        $scope.setup(3);
                        break;
                    case 8:
                        //Ultimate exit
                        //If there are threats remaining, don't enter passage
                        if (validateEscape()) {
                            passageExit = Passages.exits[index];
                            $scope.escaped = true;
                        }
                        break;
                }

                //Return linked passage.
                if (index > -1 && index < 8) {
                    passageExit = Passages.exits[index];
                    $scope.updateInfo("Travelled to Room " + (Maze.indexOf($scope.room) + 1));
                }

                return passageExit;

                function validateEscape() {
                    "use strict";
                    let message = "You escaped the Maze with " + $scope.wealth + " points!";
                    let canEscape = true;

                    Maze.forEach(function (room) {
                        room.forEach(function (row) {
                            row.forEach(function (cell) {
                                if(cell.threat){
                                    //Only allowed to escape once all Threats are dealt with
                                    canEscape = false;
                                    message = "You cannot leave while there are threats remaining";
                                }
                            });
                        });
                    });

                    $scope.updateInfo(message);
                    return canEscape;
                }
            }

            function pickupTreasure(cell) {
                "use strict";
                //If Cell has treasure
                //But isn't a 'Coin'
                if (cell.treasure && cell.treasureType !== "Coin") {
                    const treasure = getTreasure(cell.treasureType);
                    //Pick it up
                    $scope.playerTreasure.push(treasure);
                    $scope.updateInfo("Picked up the " + cell.treasureType);

                    //Update wealth
                    $scope.wealth = $scope.wealth + treasure.tValue;

                    //Remove from Cell
                    cell.treasure = false;
                    cell.treasureType = "n/a";
                }
            }

        }
    };

    $scope.setDisplay = function (cell) {
        "use strict";
        //Default style of valid
        let returnClass = "Valid";

        if (cell.passage) {
            returnClass = "Passage";
        }

        //Only set wall style if the cell has been bumped.
        if(cell.bumped && cell.threatType === "Wall"){
            returnClass = "Wall";
        }

        if (cell.treasure){
            returnClass = cell.treasureType;
        }

        if (cell.threat) {
            returnClass = cell.threatType;
        }

        if (cell === $scope.player) {
            returnClass = "Player";
        }
        return returnClass;
    };

    $scope.setTitle = function (cell) {
        "use strict";
        let title = $scope.setDisplay(cell);

        if(title === "Valid"){
            //Falsify Y to conform to Cartesian Co-Ordinate system
            title = cell.x + "," + (9 - cell.y);
        }

        return title;
    };

    $scope.dropCoin = function () {
        "use strict";
        let message = "You drop a coin";
        let dropped = false;

        //Don't allow dropping coins on Threats or Treasure
        if($scope.player.threat || $scope.player.treasure){
            message = "You can't drop a coin here...";
        } else {
            if($scope.wealth > 0){
                $scope.player.treasure = true;
                $scope.player.treasureType = "Coin";
                $scope.wealth = $scope.wealth - 1;
            } else {
                message = "You need a score of at least 1 to drop a coin";
            }
        }

        $scope.updateInfo(message);
        return dropped;
    };

    $scope.attack = function (weaponName) {
        "use strict";
        const threatName = $scope.player.threatType;
        let message = "";
        let attackOutcome = " resisted the ";

        //If there's a threat in the current Cell
        if($scope.player.threat === true){
            const threat = getThreat(threatName);
            const weapon = getWeapon(weaponName);

            //If the weapon kills the threat
            if (threat.weakTo === weapon.name && weapon.kills === threat.name){
                //Remove the threat
                $scope.player.threat = false;
                $scope.player.threatType = "n/a";
                attackOutcome = " was killed by the ";
            }

            message = "The " + threatName + attackOutcome + weaponName;

        } else {
            message = "You hit the ground with your " + weaponName + ". It doesn't do anything but you feel better";
        }

        $scope.updateInfo(message);
    };
};
