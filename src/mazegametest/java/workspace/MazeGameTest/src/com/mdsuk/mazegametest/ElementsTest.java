﻿package com.mdsuk.mazegametest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class ElementsTest {
	
    private static WebDriver driver;
    private static ChromeOptions options;

    @BeforeClass
    public static void standUp() throws InterruptedException{

        //Set driver path
        String chromedriver = "D:/libs/chromedriver.exe";

        //Set URL string
        String url = "http://127.0.0.1/mazegamesite";

        //Apply driver path to JVM
        System.setProperty("webdriver.chrome.driver", chromedriver);

        //Set options
        options = new ChromeOptions();

        //Create drivers
        driver = new ChromeDriver(options);

        //Open page
        driver.navigate().to(url);

        //Wait for page to load
        Thread.sleep(2000);
    }

    @AfterClass
    public static void tearDown() throws InterruptedException{
        //Wait before closing
        Thread.sleep(2000);
        driver.close();
    }

    /*General*/
    @org.junit.Test
    public void testPageTitle(){
        String expected = "Maze Game";
        String actual = driver.getTitle();
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void testTitleLoaded() {
        String css = "div.PageTitle";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }

    /*Main*/
    @org.junit.Test
    public void testContentLoaded() {
        String css = "main.Content";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }

    @org.junit.Test
    public void testMazeLoaded() {
        String css = "main.Content > table.MazeGame";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }
    
    @org.junit.Test
    public void testInfoLoaded() {
        String css = "span.InfoSpan";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }
    
    @org.junit.Test
    public void testInfoMessage() {
        String css = "span.InfoSpan";
        String expected = "Use the WASD or arrow keys to navigate around the maze";
        String actual = driver.findElement(By.cssSelector(css)).getText();
        assertEquals(expected, actual);
    }

    /*Aside*/
    @org.junit.Test
    public void testAsideLoaded() {
        String css = "aside.Aside";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }

    @org.junit.Test
    public void testMazeKeyLoaded() {
        String css = "aside.Aside > table.MazeKey";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }

    @org.junit.Test
    public void testAttackSpanLoaded() {
        String css = "aside.Aside > span.AttackButtonSpan";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }
    
    @org.junit.Test
    public void testAttackButtonsLoaded() {
        String club = "aside.Aside > span.AttackButtonSpan > button.AttackClub";
        String sword = "aside.Aside > span.AttackButtonSpan > button.AttackSword";
        String water = "aside.Aside > span.AttackButtonSpan > button.AttackWater";
        
        boolean clubLoaded = driver.findElement(By.cssSelector(club)).isDisplayed();
        boolean swordLoaded = driver.findElement(By.cssSelector(sword)).isDisplayed();
        boolean waterLoaded = driver.findElement(By.cssSelector(water)).isDisplayed();
        
        assertTrue(clubLoaded);
        assertTrue(swordLoaded);
        assertTrue(waterLoaded);
    }

    @org.junit.Test
    public void testScoreSpanLoaded() {
        String css = "aside.Aside > span.ScoreSpan";
        boolean loaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(loaded);
    }

    @org.junit.Test
    public void testScoreNumeric() {
        String css = "aside.Aside > span.ScoreSpan > span.ScoreValue";
        String treasureString = null;
        int lives = -1;
        try {
        	treasureString = driver.findElement(By.cssSelector(css)).getText();
            lives = Integer.parseInt(treasureString);
        } catch (NumberFormatException nfe) {
            fail("Treasure not a parsable integer: " + treasureString);
        } catch (NoSuchElementException nsee) {
			fail("Did not find element with CSS: " + css);
		}

        //Treasure will be -1 if not updated in try block
        assertFalse(lives == -1);
        //Treasure should be 0-3
        assertTrue(lives >= 0 && lives <= 3);
        //String value should be parsable as an integer
        assertNotNull(treasureString);
    }

    /*Maze Content*/
    @org.junit.Test
    public void testPlayerLoaded() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Player";
        boolean playerLoaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(playerLoaded);
    }

    @org.junit.Test
    public void testPassagesLoaded() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Passage";
        boolean exitLoaded = driver.findElement(By.cssSelector(css)).isDisplayed();
        assertTrue(exitLoaded);
    }

    /*Test Titles*/
    @org.junit.Test
    public void testTitlePassage() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Passage";
        String expected = "Passage";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");
        
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testTitlePlayer() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Player";
        String expected = "Player";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleTroll() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Troll";
        String expected = "Troll";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleWerewolf() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Werewolf";
        String expected = "Werewolf";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleVampire() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Vampire";
        String expected = "Vampire";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleGold() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Gold";
        String expected = "Gold";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleSilver() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Silver";
        String expected = "Silver";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleBronze() {
        String css = "main.Content > table.MazeGame > tbody > tr > td.Bronze";
        String expected = "Bronze";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testTitleAttackWithClubButton() {
        String css = "aside.Aside > span.AttackButtonSpan > button.AttackClub";
        String expected = "Attack with Club";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleAttackWithSwordButton() {
        String css = "aside.Aside > span.AttackButtonSpan > button.AttackSword";
        String expected = "Attack with Silver Sword";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleAttackWithWaterButton() {
        String css = "aside.Aside > span.AttackButtonSpan > button.AttackWater";
        String expected = "Attack with Holy Water";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
    @org.junit.Test
    public void testTitleDropCoinButton() {
        String css = "aside.Aside > span.DropButtonSpan > button.DropCoin";
        String expected = "Drop Coin";
        String actual = driver.findElement(By.cssSelector(css)).getAttribute("Title");

        assertEquals(expected, actual);
    }
    
}
