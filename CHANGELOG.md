# Changelog

## TODO

- [x] Add Threats / Treasures to map

- [x] Implement wealth system

- [x] Implement threats system

- [x] Implement Player inventory

- [x] Implement object dropping

- [x] Add checks to prevent escape if Threats are still present

- [x] Implement bi-directional passages and final escape

- [x] Implement Coin dropping to mark Cells

## [v0.1](https://gitlab.com/gssmds/synoptic-project/tree/v0.1/)
 
- Basic mazes defined

- Random maze is selected on page load

- Threats defined

- Treasures defined

- Movement works

- Walls + Holes added as obstacles

- Falling down a hole resets your position to the start of the current Maze

## [v0.1.1](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.1/)

- Implemented basic inventory

- Implemented object dropping

## [v0.1.2](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.2/)

- Implemented combat system

- Reduced number of Threats and Weapons for maintainability

## [v0.1.3](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.3/)

- Implemented Treasures and Threats into Maze

- Removed Holes (sadly)

## [v0.1.4](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.4/)

- Implemented wealth system

- Fix combat system

## [v0.1.5](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.5/)

- Deprecate `Functions.js`

- Rename and make `Inventory.js` consistent with `Threats.js` and `Treasures.js`

- Add start of Design documentation

## [v0.1.6](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.6/)

- Implement passages between rooms

- Implement escape validation

- Deprecate `currentCell` and `exitCell` variables

- Rename `Mazes` -> `Rooms`

## [v0.1.7](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.7/)

- Rename Rooms -> Maze

- Freeze `Threats`, `Treasures`, and `Weapons` objects

- Overhaul `Setup.js` to not require `$scope` (*significantly* less horrific)

- Start uploading and updating Documentation

- Simplify 404 page

- CSS tweaks

- Implement bi-directional passages

- Overhaul function and variable scope in `MazeGameController`

- Deprecate `$scope.version`, `$scope.passages`, and `$scope.startCell` variables

- Improve robustness and remove falling through switch statements in Threats.js, Treasures.js, and Weapons.js

- Create [`Test branch`](https://gitlab.com/gssmds/synoptic-project/tree/Test) for QUnit testing

## [v0.1.8](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.8/)

- Add `"use strict"` pragma

- Use `const` over `let` where possible

- Rename `returnCell` in `MazeGameController` `validateLocation` function to `destination` for consistency

- Always return a value from the `MazeGameController` `keyboardInput` function for validation if move was successful

- Deprecate `Start` and `Exit` CSS classes in favour of `Passage`

## [v0.1.9](https://gitlab.com/gssmds/synoptic-project/tree/v0.1.9/)

- Significant linting fixes and code quality improvements

- Use Google CDN for AngularJS rather than locally stored copies

- Implement Coin dropping

- Improve scoring system

- Rename `MazeController` `$scope.maze` -> `$scope.room` to more accurately reflect contents and purpose

- Deprecate obtrusive alerts and console.log functions in favour of `MazeController.updateInfo` function

- Add JUnit testing with Selenium WebDriver

## [v0.2](https://gitlab.com/gssmds/synoptic-project/tree/v0.2/)

- Update documentation

- Update test cases