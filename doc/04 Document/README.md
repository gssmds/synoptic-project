﻿# Limitations and Improvements

## The site is not optimised for mobile use

With the current design, I do not believe a comfortably usable mobile page is possible.

A 10x10 table will always be either too wide to fit a vertically-held mobile display, or unusably small.

The site should be usable on a mobile device held horizontally, possibly requiring the User to zoom out.

## The Maze is "upside-down"

This is a matter of perspective, but it is arguable that the Maze is upside-down.

### Explanation

- The first row of the table is the first Row in the Maze array, and contains Cells with a Y co-ordinate of 0.

- The bottom row of the table is the last Row in the Maze array, and contains Cells with a Y co-ordinate of 9.

This results in vertical movement being inconsistent with the update to the Player Cell's Y co-ordinate.

Moving down a row results in the current Player Cell's Y co-ordinate increasing.

However, the Player Cell's Y co-ordinate is consistent with its Row index in the Maze array.

This is invisible to the User, however, due to implementing [Potential Solution 2](/doc/04 Document/README.md#Potential Solution 2).

In summary the Maze has a top-left -> bottom-right structure, which is a non-standard representation of the Cartesian Co-Ordinate System.

|x/y|*0*|*1*|*2*|*3*|*4*|*5*|*6*|*7*|*8*|*9*|
|---|---|---|---|---|---|---|---|---|---|---|
|*0*|0,0|1,0|0,0|1,0|0,0|1,0|0,0|1,0|0,0|1,0|
|*1*|0,1|1,1|2,1|3,1|4,1|5,1|6,1|7,1|8,1|9,1|
|*2*|0,2|1,2|2,2|3,2|4,2|5,2|6,2|7,2|8,2|9,2|
|*3*|0,3|1,3|2,3|3,3|4,3|5,3|6,3|7,3|8,3|9,3|
|*4*|0,4|1,4|2,4|3,4|4,4|5,4|6,4|7,4|8,4|9,4|
|*5*|0,5|1,5|2,5|3,5|4,5|5,5|6,5|7,5|8,5|9,5|
|*6*|0,6|1,6|2,6|3,6|4,6|5,6|6,6|7,6|8,6|9,6|
|*7*|0,7|1,7|2,7|3,7|4,7|5,7|6,7|7,7|8,7|9,7|
|*8*|0,8|1,8|2,8|3,8|4,8|5,8|6,8|7,8|8,8|9,8|
|*9*|0,9|1,9|2,9|3,9|4,9|5,9|6,9|7,9|8,9|9,9|

### Potential Solution 1

Invert the data in each Maze array

- Cells with `Y` co-ordinate `9` are on the first row of the table

- Cells with `Y` co-ordinate `0` are on the bottom row of the table.

This results in vertical movement being consistent with the update to the Player Cell's `Y` co-ordinate.

Moving down a row results in the Player Cell's `Y` co-ordinate decreasing.

However, the Player Cell's `Y` co-ordinate is now inconsistent with its `Row` index in the `Maze` array.

This requires additional logic in the `MazeController` and more complex code.

- This would require accessing Cells from the `Maze` with the `Y` co-ordinate offset by the maximum index of the `Maze` array to account for the inversion.

In summary, the `Maze` could have a bottom-left -> top-right structure, which conforms to the Cartesian Co-Ordinate System.

|*9*|0,9|1,9|2,9|3,9|4,9|5,9|6,9|7,9|8,9|9,9|
|---|---|---|---|---|---|---|---|---|---|---|
|*8*|0,8|1,8|2,8|3,8|4,8|5,8|6,8|7,8|8,8|9,8|
|*7*|0,7|1,7|2,7|3,7|4,7|5,7|6,7|7,7|8,7|9,7|
|*6*|0,6|1,6|2,6|3,6|4,6|5,6|6,6|7,6|8,6|9,6|
|*5*|0,5|1,5|2,5|3,5|4,5|5,5|6,5|7,5|8,5|9,5|
|*4*|0,4|1,4|2,4|3,4|4,4|5,4|6,4|7,4|8,4|9,4|
|*3*|0,3|1,3|2,3|3,3|4,3|5,3|6,3|7,3|8,3|9,3|
|*2*|0,2|1,2|2,2|3,2|4,2|5,2|6,2|7,2|8,2|9,2|
|*1*|0,1|1,1|2,1|3,1|4,1|5,1|6,1|7,1|8,1|9,1|
|*0*|0,0|1,0|0,0|1,0|0,0|1,0|0,0|1,0|0,0|1,0|
|x/y|*0*|*1*|*2*|*3*|*4*|*5*|*6*|*7*|*8*|*9*|

### Potential Solution 2

Change the ```Title``` attribute returned.

Display the value of the Y co-ordinate offset by the maximum index of the `Maze` array rather than actually inverting the `Maze` array.

- This maintains the simplicity of the code design.

## It is hard to distinguish between some of the entities in the Maze

There's only so many visibly distinct colours to pick from

### Improvement

If I had time, and artistic ability, I would have liked to use pixel art images to distinguish between entities in the Maze

## The AngularJS MazeController is difficult to functionally test

The majority of the logic resides in a single function that takes a keypress event as a parameter

Testing this is difficult as it requires creating and executing an event in code, which are typically flagged as insecure and ignored by modern browsers for security purposes.

The single function contains nested functions, through which results and return values bubble

### Improvement

As the MazeController is all JavaScript, it would be possible to replicate the functions and structure in a separate testable file with some modifications

The beginning of how I would do this is found in [`/src/mazegamesite/scripts/ControllerTester.js`](/src/mazegamesite/scripts/ControllerTester.js) which I would then build QUnit tests around in the [`Test.js`](/src/mazegamesite/scripts/Test.js) file.