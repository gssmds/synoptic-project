﻿# User Guide

## What do I do?

The goal is to navigate your player through the Maze to the Exit, collecting Treasure and eliminating Threats as you go.

You want to eliminate all Threats and collect as many pieces of Treasure in as few moves as possible for a higher score.

## How do I play?

### Movement

Use the `wasd` or arrow keys on your keyboard to move your Player around the Maze.

Click the buttons on the right hand side to perform actions

#### Threats

There are three types of Threat in the Maze

- Every Threat must be eliminated before you can escape

- Eliminate a Threat by attacking it with the correct Weapon

- You have three Weapons

- It is down to you to work out which Weapon is effective against which Threat.

#### Troll

- Trolls are maroon squares

#### Werewolf

- Werewolves are brown squares

#### Vampire

- Vampires are crimson squares

### Treasure

There are three types of Treasure in the Maze, worth different amounts to increase your score.

Treasure is automatically collected when your Player steps into the same Cell as a piece of Treasure

#### Gold

- 100 points

#### Silver

- 60 points

#### Bronze

- 30 points

### Walls and Passages

Throughout the Maze are Walls and passages

- These are hidden until you bump into them

- You cannot walk through Walls

- Passages will transport you to a different Room

## What are all these coloured squares and things?

### Maze

In the centre of your screen is a 10x10 grid.

- This is the current Room of the Maze you are trying to escape.

### Player

Inside the Maze is a violet square.

- This is your Player.

### Passages

Inside the Maze are pale green squares.

- These are the Passages.

### Key

On the right hand side of the screen is a key to what the Player, Walls, Passages, Threats, and Treasures look like.

### Info

Below the Maze is an informational message that updates when you attack Threats, pickup Treasure, enter passages etc.

### Actions

Underneath the Key are two rows of buttons.

Click them to perform the specified action

### Drop Coin

As long as your Score is greater than 0, you will be able to drop a Coin as a marker to help you navigate the maze

- Dropping a Coin comes at the cost of 1 point

### Attack with:

The three buttons here correspond to an Attack action using a different weapon

- Each Weapon is only effective against a single Threat

- **To Attack a Threat you must be standing in the same Cell as it!**
 
## Score

Your aim is to escape the Maze with as high a Score as possible

Increase your Score by collecting Treasure

- Every step you take reduces your Score so be careful!