# Design

## Assumptions
- The User will have navigated to the “Maze Game” with knowledge of what the purpose of the “Maze Game” is
- The User will be using a desktop computer
- The User will have an up-to-date web browser
- The User is not visually impaired
- The User can understand the English language

## User Interface
The "Maze Game" will be a web page the User can access

The Maze itself will be a 10x10 HTML table styled with CSS and controlled with JavaScript


## Data Specification

### Maze

The Maze is a single multidimensional array ultimately containing JSON objects which represent a Cell in the Maze Game.

The Maze is defined in the [`Maze.js`](/src/mazegamesite/scripts/Maze.js) file.

- The top-level array contains three arrays.
- The second-level arrays contain ten further arrays.
- The third-level arrays contain JSON objects, which contain the data used to configure the game.

Top Level - "Maze":
- `Array[ Array[10], Array[10], Array[10] ]`

Second Level - "Room":
- `Array[ Array[10] ]`

Third Level - "Row":
- `Array[ {JSON}, {JSON}, {JSON}, ... ]`

Fourth Level - "Cell":
- `{x: INT, y: INT, threat: BOOL, threatType: STRING, treasure: BOOL, treasureType: STRING, bumped: BOOL, passage: BOOL }`

Each JSON object has the Integer properties `x` and `y`, the Boolean properties `threat`, `treasure`, and `passage`, and the String properties `threatType` and `treasureType`

Integers `x` and `y` are numeric values 0-9.

The String variables `threatType` and `treasureType` are chosen from the `Treasures` and `Threats` arrays. As JavaScript does not support enums, I have created pseudo-enums by defining them as constants and freezing the Object.

The `bumped` Boolean determines whether a `Wall` will be visible to the User or not.

A `Wall` is hidden by default and only reveals once the User attempts to move there.
 
The `passage` Boolean determines if a Cell is a passage to another `Room`

### Passages

Passages are defined in the [`Passages.js`](/src/mazegamesite/scripts/Passages.js) file within the the `Passages` JSON object.

`Passages` is a JSON object with properties of `starts` and `exits` the values of which are arrays of Cells

### Threats, Treasures, and Weapons

The Threats, Treasures, and Weapons are defined in the [`Threats.js`](/src/mazegamesite/scripts/Threats.js), [`Treasures.js`](/src/mazegamesite/scripts/Treasures.js), and [`Weapons.js`](/src/mazegamesite/scripts/Weapons.js) files respectively.

Again, they are arrays ultimately containing JSON objects

All of these JSON objects have a String property of `name` and a second value relevant to their purpose.

- `{name: "Troll", weakTo: "Club"}`

- `{name: "Gold", tValue: 100}`

- `{name: "Club", kills: "Troll"}`

Each of these has a corresponding getter to retrieve an instance of an object.

## Links

*See "Data Design" and "Data Explanation" Sheets in [Design.ods](/doc/01 Design/Design.ods)*

*See "UI Design" and "UI Explanation" Sheets in [Design.ods](/doc/01 Design/Design.ods)*