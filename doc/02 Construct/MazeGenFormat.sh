#!/usr/bin/env bash

set -eu

source="raw.txt"
format="../../src/mazegamesite/scripts/Maze.js"
exec > "${format}" 2>&1

#Open Maze declaration
echo "const Maze = ["

#For every line
while read -r line
do
	#Open a new Array
	#Switch tabs for new lines
	#Switch single quotes for double quotes
	#Close new Array
	echo "["
	echo "${line}" | tr "'" "\"" | tr "\t" "\n"
	echo "],"
done < "${source}"

#Close Maze declaration
echo "];"
