﻿# Construct

## Maze Game

The Maze Game is written with HTML, CSS, JavaScript, and AngularJS

The code for the Maze Game can be found in the [`/src/mazegamesite`](/src/mazegamesite) directory

## Utilities

### MazeGen.ods
This is an Open Document Spreadsheet 

I used this to quickly design Mazes and produce data in the format specified in [Design](/doc/01%20Design)

It uses conditional formatting to colour Cells for visual Maze design, and a Basic macro to produce the data.

### MazeGenMacro.txt
This is the Basic macro used in the spreadsheet

It reads the contents, and produces a JSON representation, of each Cell in the specified range

### MazeGenFormat.sh

This formats the JSON data produced by the MazeGenMacro into the multi-dimensional array of [`Maze.js`](/src/mazegamesite/scripts/Maze.js) for a fully-automated maze construction workflow.