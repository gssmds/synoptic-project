# Test

## Validation Linting

At regular intervals throughout development, I used online linting tools and the built-in validation of my chosen editor to validate the syntax and style of my HTML, CSS, and JavaScript code


## Unit Testing

I used the Selenium WebDriver and Java bindings and the QUnit JavaScript unit testing framework to perform automated testing against my code.


### Links

- [HTML Validation](https://validator.w3.org/#validate-by-upload)
- [CSS Validations](http://jigsaw.w3.org/css-validator/#validate-by-upload)
- [JavaScript Validation](https://jslint.com/)
- [WebStorm IDE](https://www.jetbrains.com/webstorm/)
- [Selenium WebDriver](https://docs.seleniumhq.org/projects/webdriver/)
- [QUnit](https://qunitjs.com/)

---

*See [`/src/mazegametest`](/src/mazegametest)*

*See [`/src/mazegamesite/qunit.html`](/src/mazegamesite/qunit.html)*

*See [`/src/mazegamesite/scripts/Test.js`](/src/mazegamesite/scripts/Test.js)*

*See [QUnit Results](/doc/03%20Test/QUnit_Results.PNG)*

*See [JUnit Results](/doc/03%20Test/JUnit_Results.PNG)*