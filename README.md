﻿# Synoptic Project

## Project B - Maze Game

### Specification
- [Specification](/doc/00%20Specification/Software%20Development%20Technician%20Project%20B%20V1.0%20QA01.pdf)

### Design
- [Requirements and assumptions](/doc/01%20Design/README.md#assumptions)
- [User Interface design](/doc/01%20Design/Design.ods) - Sheet: *UI Design*
- [User Interface explanation](/doc/01%20Design/Design.ods) - Sheet: *UI Explanation*
- [Data specification](/doc/01%20Design/README.md#data-specification)

### Construction
- [HTML / CSS / JavaScript / AngularJS code](/src/mazegamesite)
- [Playable version](http://gssmds.ddns.net:8448/synopticproject/)

### Test
- [Selenium WebDriver JUnit test cases](/src/mazegametest/java/workspace/MazeGameTest/src/com/mdsuk/mazegametest/ElementsTest.java)
- [QUnit test cases](/src/mazegamesite/scripts/Test.js)
- [Test execution logs](http://gssmds.ddns.net:8448/synopticproject/qunit.html)

### Document
- [Design and Implementation Limitations and Improvements](/doc/04%20Document/README.md)
- [User guide](/doc/04%20Document/UserGuide.md)